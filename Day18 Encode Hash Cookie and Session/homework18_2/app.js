const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const mysql = require('mysql2/promise')
const koaBody = require('koa-body')
const bcrypt = require('bcrypt')
const session = require('koa-session')
const flash = require('koa-flash')
const serve = require('koa-static')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
  })
const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'pikkanode'
})
app.keys = ['some sadfk dsjkl fjsdlak;j fklsdj lkjfklsdjf ldks;ssdfs'];
const sessionStore = {};

const CONFIG = {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    maxAge: 86400000 * 7,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
    renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key];
        },
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess;
        },
        destroy(key) {
            delete sessionStore[key];
        }
    }
};
 
  router.get('/', async ctx=>{
    const [rows] = await pool.query('select * from pictures')
    const dataEJS ={'picture':rows}
    await ctx.render('index',dataEJS)
})
router.get('/signup',getSignUpHandler)
router.post('/signup',postSignUpHandler)
router.get('/signin',getSignInHandler)
router.post('/signin',postSignInHandler)
router.get('/create',uploadGetHandler)
router.post('/create',uploadPostHandler)

async function getSignUpHandler(ctx, next){
    const data = {'flash':ctx.flash}
    console.log(data)
    await ctx.render('form_sign_up',data)
    ctx.variable = 'signup'
    await next()
}

async function postSignUpHandler(ctx){
    const email = ctx.request.body.email
    const password = ctx.request.body.password
    const confirmPassword = ctx.request.body.confirm_password
    console.log('password'+password)
    console.log('confirm password'+confirmPassword)
    if(password!==confirmPassword){
        ctx.flash ={error: 'password not match'}
        ctx.redirect('/signup')
        return
    }
const hashedPassword = await bcrypt.hash(password,10)
 await pool.query(`insert into users(email, password) values('${email}', '${hashedPassword}')`)
 
 console.log('email:'+email)
 console.log('password'+password)
 console.log('hashed password'+hashedPassword)
ctx.status = 302
ctx.flash= {success:'signup success'}
ctx.redirect('/signup')
}

async function getSignInHandler(ctx, next){
   
    const data ={'flash': ctx.flash}
    console.log(data)
    await ctx.render('form_sign_in',data)
   ctx.variable = 'signin'
   await next()
}
async function postSignInHandler(ctx){
    const email = ctx.request.body.email
    const password = ctx.request.body.password

const [rows] = await pool.query(`select * from users where email ='${email}' limit 1 `)
//check email
if(rows.length<=0)
{
   //console.log('wrong email')
    ctx.flash = {error: 'wrong email'}
    ctx.redirect('/signin')
   return 
}
for(let i in rows){
 const checkPassword = await bcrypt.compare(password, rows[i].password)   
 //check password
if(!checkPassword){
    //console.log('wrong password')
    ctx.flash = {error: 'wrong password'}
    ctx.redirect('/signin')
    return 
   }
   ctx.session.userID = rows[i].id
   console.log('session userID:'+ctx.session.userID)
  //console.log('login success')
  ctx.flash = {success:'login success'}
  ctx.redirect('/signin')
   return 
   }

}

async function uploadGetHandler(ctx, next){
 await ctx.render('/upload')
 ctx.variable = 'upload'
 await next()
}

async function  uploadPostHandler(ctx){
    const fileName = uuidv4() + ctx.request.files.photo.name
    const folderDir = path.join(__dirname, 'public/images')
    const fileDir = path.join(folderDir, `${fileName}` )
    await fs.rename(ctx.request.files.photo.path, fileDir)
    const caption = ctx.request.body.caption
    const userID = ctx.session.userID
    console.log('useriD:'+userID)
    await pool.query(`insert into pictures (id, caption, created_by) values ('${fileName}', '${caption}', ${userID})`)
}

async function checkSignin(ctx, next){
    if(ctx.variable==='signin'){
        if(ctx.session.userID){
            ctx.redirect('/create')
    }
    }
   else{
    await next()
   }
  
}

async function checkSignup(ctx, next){
    if(ctx.variable==='signup'){
        if(ctx.session.userID){
            ctx.redirect('/create')
    }
    }
   else{
    await next()
   }
}

async function checkUpload(ctx, next){
    if(ctx.variable==='upload'){
        if(!ctx.session.userID){
            ctx.redirect('/signin')
    }
    }
   else{
    await next()
   }
}

app.use(session(CONFIG, app))
app.use(flash())
app.use(koaBody({multipart:true}))
app.use(checkSignin)
app.use(checkSignup)
app.use(checkUpload)
app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)