const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const mysql = require('mysql2/promise')
const koaBody = require('koa-body')
const bcrypt = require('bcrypt')
const session = require('koa-session')
const cors = require('@koa/cors')

   
const app = new Koa()
const router = new Router()
app.use(cors({
    origin: '*',
    allowMethods: [ 'POST','GET'],
    allowHeaders: ['Content-Type'],
    maxAge: 300
   }))

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
  })
const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'pikkanode'
})
app.keys = ['some sadfk dsjkl fjsdlak;j fklsdj lkjfklsdjf ldks;ssdfs'];
const sessionStore = {};

const CONFIG = {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    maxAge: 86400000 * 7,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
    renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key];
        },
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess;
        },
        destroy(key) {
            delete sessionStore[key];
        }
    }
};
 
router.get('/signup',getSignUpHandler)
router.post('/api/v1/auth/signup',postSignUpHandler)
router.get('/signin',getSignInHandler)
router.post('/api/v1/auth/signin',postSignInHandler)
router.get('/signout', getSignOutHandler)
router.post('/api/v1/auth/signout', postSignOutHandler)
async function getSignUpHandler(ctx){
    await ctx.render('form_sign_up')

}

async function postSignUpHandler(ctx){
    const email = ctx.request.body.email
    const password = ctx.request.body.password
    const confirmPassword = ctx.request.body.confirm_password
    //console.log('password'+password)
   // console.log('confirm password'+confirmPassword)
    
    if(!email){
        ctx.status=400
         ctx.body= {"error": "invalid email"}
         return
    }
    const [rows] = await pool.query(`select * from users where email = ?`,[email])
    for(let i in rows){
        if(email === rows[i].email){
            ctx.status=400
            ctx.body ={"error": "email already used"}
            return
        }
    }
    if(password!==confirmPassword){
        ctx.status=400
       ctx.body= {"error": "password not match"}
       return 
    }
    if(password.length<6){
        ctx.status=400
        ctx.body= {"error": "password too short"}
        return 
    }

const hashedPassword = await bcrypt.hash(password,10)
const [rows2] = await pool.query('insert into users(email, password) values(?,?)',[email, hashedPassword])
 console.log('email:'+email)
 console.log('password'+password)
 console.log('hashed password'+hashedPassword)
ctx.status = 200
ctx.body ={"userId":rows2.insertId}
}

async function getSignInHandler(ctx){
    await ctx.render('form_sign_in')  
}

async function postSignInHandler(ctx){
    const email = ctx.request.body.email
    const password = ctx.request.body.password

const [rows] = await pool.query(`select * from users where email = ? `,[email])
//check email
if(rows.length<=0)
{
   ctx.status=400
   return ctx.body = {"error":"wrong email"}
}
for(let i in rows){
 const checkPassword = await bcrypt.compare(password, rows[i].password)   
 //check password
if(!checkPassword){
    //console.log('wrong password')
    ctx.status=400
    return ctx.body = {"error":"wrong password"}
   }
   ctx.session.userID = rows[i].id
   console.log('session userID:'+ctx.session.userID)
  //console.log('login success')
  ctx.status=200
  return ctx.body = {"success":"Login complete"}
   }

}

async function getSignOutHandler(ctx){
 await ctx.render('form_sign_out')
}

async function postSignOutHandler(ctx){
    delete ctx.session.userID
    ctx.status = 200
    return ctx.body ={}
}

app.use(session(CONFIG, app))
app.use(koaBody())
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)