const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const mysql = require('mysql2/promise')
const koaBody = require('koa-body')
const serve = require('koa-static')
const uuidv4 = require('uuid/v4')
const fs = require('fs-extra')
const cors = require('@koa/cors')
const session = require('koa-session')
const bcrypt = require('bcrypt')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
  })
const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'pikkanode'
})
app.use(cors({
    origin: '/create',
    allowMethods: [ 'POST','GET'],
    allowHeaders: ['Content-Type'],
    maxAge: 300
   }))
    
   app.keys = ['some sadfk dsjkl fjsdlak;j fklsdj lkjfklsdjf ldks;ssdfs'];
const sessionStore = {};

const CONFIG = {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    maxAge: 86400000 * 7,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
    renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key];
        },
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess;
        },
        destroy(key) {
            delete sessionStore[key];
        }
    }
};

router.get('/api/v1/pikka', getPictureHandler)
router.get('/create', getUploadHandler)
router.post('/api/v1/pikka', postUploadHandler)
router.get('/signin',getSignInHandler)
router.post('/api/v1/auth/signin',postSignInHandler)

async function getPictureHandler(ctx){
    const [rows] = await pool.query('select * from pictures')
    ctx.status=200
    ctx.body={'list':rows}
 
}

async function getUploadHandler(ctx){
    await ctx.render('upload')
}

async function  postUploadHandler(ctx){
    
    const fileName = uuidv4() + ctx.request.files.photo.name
    const folderDir = path.join(__dirname, 'public/images')
    const fileDir = path.join(folderDir, `${fileName}` )
    await fs.rename(ctx.request.files.photo.path, fileDir)
    const caption = ctx.request.body.caption
    const userID = ctx.session.userID
    console.log(userID)
    console.log(fileName)
    console.log(caption)
    if(!userID){
        ctx.stutus= 401
        return ctx.body= { "error": "unauthorized"}
    }
    if(!ctx.request.files.photo.name){
        ctx.status=400
        return ctx.body = {"error": "picture required"}
    }
    if(!caption){
        ctx.status=400
        return ctx.body = {"error": "caption required"}
    }
   await pool.query(`insert into pictures (id, caption, created_by) values (?, ?, ?)`,[fileName, caption, userID])
   console.log('insert complete')
   const [rows2] = await pool.query('select * from pictures where id=?',[fileName])
   for(let i in rows2){
       ctx.status = 200
       ctx.body ={ 'id':rows2[i].id, 'caption':rows2[i].caption, 
       'createdAt':rows2[i].created_at, 'createdBy':rows2[i].created_by}
   }
}

async function getSignInHandler(ctx){
    await ctx.render('form_sign_in')  
}

async function postSignInHandler(ctx){
    const email = ctx.request.body.email
    const password = ctx.request.body.password

const [rows] = await pool.query(`select * from users where email = ? `,[email])
//check email
if(rows.length<=0)
{
   ctx.status=400
   return ctx.body = {"error":"wrong email"}
}
for(let i in rows){
 const checkPassword = await bcrypt.compare(password, rows[i].password)   
 //check password
if(!checkPassword){
    //console.log('wrong password')
    ctx.status=400
    return ctx.body = {"error":"wrong password"}
   }
   ctx.session.userID = rows[i].id
   console.log('session userID:'+ctx.session.userID)
  //console.log('login success')
  ctx.status=200
  return ctx.body = {"success":"Login complete"}
   }

}
app.use(session(CONFIG, app))
app.use(koaBody({multipart:true}))
app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)