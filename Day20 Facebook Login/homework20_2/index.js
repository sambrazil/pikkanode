const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')
const koaBody = require('koa-body')
const bcrypt = require('bcrypt')
const session = require('koa-session')
const flash = require('koa-flash')
const path = require('path')

const app = new Koa()
const router = new Router()

const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'pikkanode'
})

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
  })
  app.keys = ['some sadfk dsjkl fjsdlak;j fklsdj lkjfklsdjf ldks;ssdfs'];
const sessionStore = {};

const CONFIG = {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    maxAge: 86400000 * 7,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
    renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key];
        },
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess;
        },
        destroy(key) {
            delete sessionStore[key];
        }
    }
};
  router.get('/', async ctx=>{
      await ctx.render('index')
  })
  router.get('/facebook_signup', getFacebookSignup)
  router.post('/facebook_signup',postFacebookSignup)
  router.get('/signup',getSignUpHandler)
  router.post('/signup',postSignUpHandler)
  router.get('/signin',getSignInHandler)
  router.post('/signin',postSignInHandler)



  async function getFacebookSignup(ctx){
    await ctx.render('facebook_signup')
}

async function postFacebookSignup(ctx){
    const id = ctx.request.body.id
    const firstname = ctx.request.body.firstName
    const lastname = ctx.request.body.lastName
    const email = ctx.request.body.email 
    const password = ctx.request.body.password
    const username = ctx.request.body.username
    const confirmPassword = ctx.request.body.confirmPassword
    const hashPassword = await bcrypt.hash(password,10)
    //console.log(id)
    //console.log(firstname)
    //console.log(lastname)
    //console.log(email)
   //console.log(password)
  // console.log(hashPassword)
    if(!id){
        ctx.body =`{error:require id} <br/> <a href='/facebook_signup'>Back to sign up</a> `
        return
    }
    if(!username){
        ctx.body =`{error:require username} `
        return
    }
    if(!email){
        ctx.body =`{error:require email} `
        return
    }
    if(!firstname){
        ctx.body =`{error:require firstname}`
        return
    }
    if(!lastname){
        ctx.body =`{error:require lastname} `
        return
    }
  if(password!== confirmPassword){
      ctx.body =`{error:password no match} `
      return
  }
  
  const [rows2] = await pool.query('select * from facebook_register where username=?',[username])
   if(rows2.length>0){
    ctx.body =`{error:username already used} `
    return
   }

    const [rows] = await pool.query('select * from facebook_register where facebook_user_id=?',[id])
    if(rows.length <=0){
   await pool.query('insert into facebook_register (facebook_user_id,firstname,lastname,username,email,password) values (?,?,?,?,?,?)',
   [id,firstname,lastname,username,email,hashPassword])
   ctx.body = 'Sign up complete'
     return
   } 
   else{
    ctx.body =`{error:facebook already sign up} `
    return
   }
 }
 
 async function getSignUpHandler(ctx){
    await ctx.render('signup')
}
async function postSignUpHandler(ctx){
    const username = ctx.request.body.username
    const firstname = ctx.request.body.firstName
    const lastname = ctx.request.body.lastName
    const email = ctx.request.body.email 
    const password = ctx.request.body.password
    const confirmPassword = ctx.request.body.confirmPassword
    const hashPassword = await bcrypt.hash(password,10)
    console.log('password'+password)
    console.log('confirm password'+confirmPassword)
    if(!username){
        ctx.body =`{error:require username} `
        return
    }
    if(!email){
        ctx.body =`{error:require email} `
        return
    }
    if(!firstname){
        ctx.body =``
        return
    }
    if(!lastname){
        ctx.body =`{error:require lastname} `
        return
    }
  if(password!== confirmPassword){
      ctx.body =`{error:password no match} `
      return
  }
  
  const [rows2] = await pool.query('select * from facebook_register where username=?',[username])
   if(rows2.length>0){
    ctx.body =`{error:username already used} `
    return
   }
   const [rows3] = await pool.query('select * from facebook_register where email=?',[email])
   if(rows3.length>0){
    ctx.body =`{error:email already used} `
    return
   }

   await pool.query('insert into facebook_register (facebook_user_id,firstname,lastname,username,email,password) values (?,?,?,?,?,?)',
   ['',firstname,lastname,username,email,hashPassword])
   ctx.body = 'Sign up complete'
     
 
}

async function getSignInHandler(ctx){
   
    const data ={'flash': ctx.flash}
    console.log(data)
    await ctx.render('signin',data)
}
async function postSignInHandler(ctx){
    const username = ctx.request.body.username
    const password = ctx.request.body.password

const [rows] = await pool.query(`select * from facebook_register where username =? `,[username])
//check username
if(rows.length<=0)
{
   //console.log('wrong username')
    ctx.flash = {error: 'wrong username'}
    ctx.redirect('/signin')
   return 
}
for(let i in rows){
 const checkPassword = await bcrypt.compare(password, rows[i].password)   
 //check password
if(!checkPassword){
    //console.log('wrong password')
    ctx.flash = {error: 'wrong password'}
    ctx.redirect('/signin')
    return 
   }
  //console.log('login success')
  ctx.flash = {success:'login success'}
  ctx.redirect('/signin')
   return 
   }
}  

app.use(session(CONFIG, app))
app.use(flash())
app.use(koaBody())
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)