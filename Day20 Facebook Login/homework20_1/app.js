const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const mysql = require('mysql2/promise')
const session = require('koa-session')
const koaBody = require('koa-body')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: false,
    viewExt: 'ejs',
    cache: false
  })
const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'pikkanode'
})

app.keys = ['some sadfk dsjkl fjsdlak;j fklsdj lkjfklsdjf ldks;ssdfs'];
const sessionStore = {};

const CONFIG = {
    key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
    maxAge: 86400000 * 7,
    overwrite: true, /** (boolean) can overwrite or not (default true) */
    httpOnly: true, /** (boolean) httpOnly or not (default true) */
    signed: true, /** (boolean) signed or not (default true) */
    rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
    renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
    store: {
        get(key, maxAge, { rolling }) {
            return sessionStore[key];
        },
        set(key, sess, maxAge, { rolling, changed }) {
            sessionStore[key] = sess;
        },
        destroy(key) {
            delete sessionStore[key];
        }
    }
};
router.get('/', getFacebookLogin)
router.post('/',postFacebookLogin)
router.get('/profile',getProfile)

async function getFacebookLogin(ctx){
    await ctx.render('facebook_login')
}

async function postFacebookLogin(ctx){
   const id = ctx.request.body.id
   const firstName = ctx.request.body.firstName
   const lastName = ctx.request.body.lastName
  
   console.log(id)
   console.log(firstName)
   console.log(lastName)
   const [rows] = await pool.query('select * from facebook_users where facebook_user_id=?',[id])
  if(rows.length<=0){
 await pool.query('insert into facebook_users (facebook_user_id,firstname,lastname) value(?,?,?)',[id,firstName,lastName])
  }
  ctx.session.userId = id
  ctx.redirect('/profile')
}

async function getProfile(ctx){
  const [rows] = await pool.query('select * from facebook_users where facebook_user_id=?',[ctx.session.userId])
  const data =  {'data':rows}
  await ctx.render('facebook_profile',data)

}

app.use(session(CONFIG,app))
app.use(koaBody())
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)