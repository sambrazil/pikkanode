import React from 'react'
class SiginIn extends React.Component{
 state = {
     email:'',
     password:'',
     isAuth:false
 }
 HandlerOnchange = e =>{
     this.setState({
         [e.target.name]:e.target.value
     })
 }
 HandlerSubmit = async e =>{
     e.preventDefault()
    const {email,password,isAuth} = this.state
    if(email===''){
        alert('no email')
        document.form.email.focus()
        return
    }
    if(password===''){
        alert('no password')
        document.form.password.focus()
        return
    }
    try{
     const option = {
          method:'post',
          headers:{'content-type':'application/json'},
          body: JSON.stringify({
              email:email,
              password:password
          }) 
      }
       const response = await fetch('http://localhost:3001/api/v1/auth/signin',option)
       const data = await response.json()
       const status = await response.status
       console.log(data)
       if(status==200){
           this.setState({
               isAuth:true
           })
       }

    }
    catch (err){
        console.error(err)
    }
 }
render(){
    const {email,password,isAuth} = this.state
    
    return(
        <form method="POST" name='form' onSubmit={this.HandlerSubmit}>
        <label htmlFor='email'>email</label>
        <input type="email" name="email" id='email' value={email} onChange={this.HandlerOnchange} />
        <br/>
        <label htmlFor='password'>password</label>
        <input type="password" name='password' id='password' value={password} onChange={this.HandlerOnchange} />
        <br/>
        <button>submit</button><br/>
        {isAuth?<span>isAuth = true</span>:<span>isAuth = false</span>}
    </form>
    )

 }
}
export default SiginIn