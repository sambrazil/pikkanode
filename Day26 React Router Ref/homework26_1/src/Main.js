import React from 'react'
import {Switch,Route} from 'react-router-dom'
import SignUp from './SignUp'
const Main =()=>(
    <Switch>
        <Route path='/signup' exact component={SignUp} />
    </Switch>
)

export default Main