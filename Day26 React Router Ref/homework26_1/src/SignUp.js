import React from 'react'

class SignUp extends React.Component{
  state ={
             email:'',
             password:'',
             confirm_password:''
           }
    OnChangeHanlder = (e)=>{
      this.setState({
        [e.target.name]:e.target.value
      })
    }       
   SubmitHandler =  async (e)=>{
      e.preventDefault()
      const {email,password,confirm_password} = this.state
      if(email===''){
        alert('no email')
        document.form.email.focus()
        return
      }
      if(password===''){
        alert('no password')
        document.form.password.focus()
        return
      }
      if(confirm_password===''){
       alert('no confirm password')
       document.form.confirm_password.focus()
       return
      }
      if(password!==confirm_password){
        alert('password and confirm password no match')
        document.form.confirm_password.focus()
        return
      }
     
       try{
         const option ={
           method:'post',
           headers:{'content-type':'application/json'},
           body:JSON.stringify({
              email: email,
              password: password,
              confirm_password: confirm_password
           })
         }
         const response = await fetch('http://localhost:3001/api/v1/auth/signup',option)
         const data =  await response.json()
         console.log(data)

       } 
       catch (err){
         console.error(err)
       }
     
    }
    render(){
      const {email,password,confirm_password} = this.state
      return( <form name='form' method="POST" onSubmit={this.SubmitHandler} >
        <label htmlFor='email'>email</label>
        <input type="email" name='email' id='email' value={email} onChange={this.OnChangeHanlder}/>
        <br/>
        <label htmlFor='password'>password </label>
        <input type="password" name='password' id='password' value={password} onChange={this.OnChangeHanlder} />
        <br/>
        <label htmlFor='confirm_password'>confirm password</label>
        <input type="password"name='confirm_password' id='confirm_password' value={confirm_password} onChange={this.OnChangeHanlder}/>
        <br/>
        <button>sign up</button>
    </form>
      )
    }
}
export default SignUp