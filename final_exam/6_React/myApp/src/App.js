import React from 'react'
import AddForm from './AddForm'
import ListItem from './ListItem'

class App extends React.Component{
 state = {
  data:[]
 }
onSubmit=  async (e)=>{
  console.log('input'+e.name)
   try {
    const response = await fetch('https://api.github.com/search/repositories?q=react')
    const data = await response.json()
    const arr = [...data.items]
     

  for(let i in arr) {
   if(arr[i].name === e.name){
    this.setState({
      data:[...this.state.data,{...arr[i]}]
    })
  } 

console.log(this.state.data)

  }
  } catch (err) {
    console.log(err)
  }
 
 }
    render(){
      return(
        <div>    
          <AddForm onSubmit={this.onSubmit} />
          <ListItem data1={this.state.data}/>
        
        </div>
      )
    }
}
export default App
