import React from 'react'
class AddForm extends React.Component{
  state = {
    name:''
  }
  HandleChange = e=>{
    this.setState({
      [e.target.name]:e.target.value
    })
  }
 HandleSubmit = e=>{
   e.preventDefault()
   this.props.onSubmit(this.state)
   this.setState({
    name:''
   })
 } 
 render(){
   const {name} = this.state
   return(
     <div>
     <form onSubmit={this.HandleSubmit}>
       <label htmlFor='name'>name</label><input type='text' id='name' name='name' value={name} onChange={this.HandleChange}/><br/>
       <button>Search</button>
     </form>
     </div>
   )
 }
}
export default AddForm
