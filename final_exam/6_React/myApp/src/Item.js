import React from 'react'
const Item =(props)=>(
    <div>
        "id": {props.data2.id} <br/>
      "node_id" {props.data2.node_id} <br/>
      "name":{props.data2.name} <br/>
      "full_name": {props.data2.full_name}  <br/>
      "private": {props.data2.private? <span>true</span> :<span>false</span> } <br/>
      "owner": [ <br/>
        "login":  {props.data2.owner.login} <br/>
        "id": {props.data2.owner.id} <br/>
        "node_id": {props.data2.owner.node_id} <br/>
        "avatar_url": {props.data2.owner.avatar_url} <br/>
        "gravatar_id": {props.data2.owner.gravatar_id? <span>true</span> :<span>""</span>} <br/>
        "url": {props.data2.owner.url} <br/>
        "html_url": {props.data2.owner.html_url} <br/>
        "followers_url": {props.data2.owner.followers_url} <br/>
        "following_url": {props.data2.owner.following_url} <br/>
        "gists_url": " {props.data2.owner.gists_url} <br/>
        "starred_url": " {props.data2.owner.starred_url} <br/>
        "subscriptions_url": {props.data2.owner.subscriptions_url} <br/>
        "organizations_url": {props.data2.owner.organizations_url} <br/>
        "repos_url": {props.data2.owner.repos_url} <br/>
        "events_url": {props.data2.owner.events_url} <br/>
        "received_events_url": {props.data2.owner.received_events_url} <br/>
        "type": {props.data2.owner.type} <br/>
        "site_admin": {props.data2.owner.site_admin? <span>true</span> :<span>false</span>} <br/>
    ]
    <br/><br/><br/><br/><br/>
        
    </div>
)
export default Item
