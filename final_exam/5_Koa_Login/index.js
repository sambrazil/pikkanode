const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')
const session = require('koa-session')
const koaBody = require('koa-body')
const bcrypt = require('bcrypt')
const mysql = require('mysql2/promise')

const app  = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
  })
  const sessionStore = {}
  app.keys = ['some secret hurr'];
const CONFIG = {
  key: 'koa:sess', /** (string) cookie key (default is koa:sess) */
  /** (number || 'session') maxAge in ms (default is 1 days) */
  /** 'session' will result in a cookie that expires when session/browser is closed */
  /** Warning: If a session cookie is stolen, this cookie will never expire */
  maxAge: 86400000,
  autoCommit: true, /** (boolean) automatically commit headers (default true) */
  overwrite: true, /** (boolean) can overwrite or not (default true) */
  httpOnly: true, /** (boolean) httpOnly or not (default true) */
  signed: true, /** (boolean) signed or not (default true) */
  rolling: false, /** (boolean) Force a session identifier cookie to be set on every response. The expiration is reset to the original maxAge, resetting the expiration countdown. (default is false) */
  renew: false, /** (boolean) renew session when session is nearly expired, so we can always keep user logged in. (default is false)*/
  get(key, maxAge, { rolling }){
     return sessionStore[key]
  },
set(key, sess, maxAge, { rolling, changed }){
     sessionStore[key] = sess
},
destroy(key){
  delete sessionStore[key]
}
};

const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'koa_login'
})

router.get('/register',async(ctx,next)=>{
    await ctx.render('register')
    await next()
})

router.post('/register_complete',async(ctx,next)=>{
    const {username, password, confirm_password, mobile_phone, confirm_mobile} = ctx.request.body
    const hashPassword = await bcrypt.hash(password,10)
    const[result] = await pool.query('INSERT INTO user(username,password,mobile_phone) VALUES (?,?,?)',[username,hashPassword,mobile_phone])
    ctx.body ='Register Sucess \n'+'Your user id: '+result.insertId+'\n Your username: '+username 
    await next()
})

router.get('/login',async(ctx,next)=>{
    await ctx.render('login')
    await next()
})

router.post('/login_complete',async(ctx,next)=>{
    const {username, password}= ctx.request.body
    const [result] = await pool.query('SELECT * from user where username =?',[username])
    //console.log(result[0].password , result[0].username,password)
    const comparePassword = await bcrypt.compare(password, result[0].password)
     if(!comparePassword){
         
        await ctx.render('login_fail')
     }
     else{
        ctx.body='Login Success'
        ctx.session.userId = result[0].id
        ctx.redirect('/profile')
     } 
    await next()
})

router.get('/profile', async (ctx,next)=>{
    const [result] =await pool.query('SELECT * FROM user where id=?',[ctx.session.userId])
    const data = {
      userId:result[0].id,
      phone:result[0].mobile_phone
    }
    await ctx.render('profile',data)
    await next()
})
router.get('/logout',async(ctx,next)=>{
    ctx.session = null
    await ctx.render('logout')
    await next()
})

async function CheckLogin(ctx,next){
  if(ctx.path ==='/profile'){
     if(!ctx.session || !ctx.session.userId){
         await ctx.render('login')
     }
     else{
         await next()
     }
  }
  else{
      await next()
  }
}

app.use(session(CONFIG, app))
app.use(koaBody())
app.use(CheckLogin)
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)