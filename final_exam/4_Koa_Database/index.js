const Koa = require('koa')
const path = require('path')
const Router = require('koa-router')
const render = require('koa-ejs')
const serve = require('koa-static')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
 
  })

const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'exam'
})

router.get('/',async(ctx,next)=>{
  //select e.id, e.firstname, e.lastname, j.job_name from employee e inner join job j on  e.job_id = j.id ;
  //select j.id,j.job_name  from job j left join employee e on j.id = e.job_id where e.id is null ;  
const[result1] = await pool.query('select e.id, e.firstname, e.lastname, j.job_name from employee e inner join job j on  e.job_id = j.id')
const [result2] = await pool.query('select j.id,j.job_name  from employee e  right join job j on  e.job_id= j.id  where e.id is null')
// select j.id,j.job_name  from employee e  right join job j on  e.job_id= j.id  where e.id is null
const data = {
    'table1':result1,
    'table2':result2
}
//console.log(data.table1)
await ctx.render('table',data)
await next()
})


  app.use(serve(path.join(__dirname,'public')))
  app.use(router.routes())
  app.use(router.allowedMethods())
  app.listen(3000)
 