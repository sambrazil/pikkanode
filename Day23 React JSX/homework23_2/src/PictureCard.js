import React from 'react'
class PictureCard extends React.Component {
    render(){
    let content =[]
    const arr = [{id:1, date:'16-01-2001', like:0, comment:0}, {id:2, date:'17-01-2001', like:1, comment:1}, 
    {id:2, date:'17-01-2001', like:1, comment:1},{id:3, date:'18-01-2001', like:3, comment:3},{id:4, date:'20-01-2001', like:4, comment:4}]
   
    arr.forEach((picture,i)=>{
        content.push(<div key={picture.id} className='col-3'><img src='/images/picture.png'/><br/> Date:{picture.date}<br/> 
        Like:{picture.like}<br/> Comment:{picture.comment} </div>)
        if((i+1)%4==0){
            content.push(<div className='w-100'></div>)
        }
    })
    return <div className='row'>{content}</div>
  }
  
}
export default PictureCard
