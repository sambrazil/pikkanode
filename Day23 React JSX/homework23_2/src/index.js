import React from 'react'
import ReactDOM from 'react-dom'
import NavBar from './NavBar'
import PictureCard from './PictureCard'
import Footer from './Footer';
const Result = ()=>{
    return <div><NavBar/><PictureCard/><Footer/></div>
}

ReactDOM.render(<Result/>,document.getElementById('root'))
