import React from 'react'
import ListItem from './ListItem'
class Pikka extends React.Component{
    state = {
        isLoading:true,
        mounted:true,
        pikka:[]
        
    }
   
    
   
    async componentDidMount(){
        if(this.state.mounted){
        try{
         const response = await fetch('http://localhost:3001/api/v1/pikka')
         const data = await response.json()
        this.setState({
            pikka:[...data.list]
        })
        
       }
       catch(err){
           console.error(err)
       }  
       finally{
           this.setState({
            isLoading:false
           })     
       }
    }
}
    componentWillUnmount(){
        this.setState({
            mounted:false
        })
    }
    render(){
         const{isLoading,pikka} = this.state
        return(<div>
            {isLoading?<div><img src='/images/loading.jpg' alt='loading'/></div> :<div></div>}
            {<ListItem pikka={pikka} />}
            </div>
        )
    }
}
export default Pikka