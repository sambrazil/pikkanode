import React from 'react'
import Pikka from './Pikka'
import {Switch,Route} from 'react-router-dom'
const Main =()=>(
    <Switch>
     <Route path='/' exact component={Pikka} />  
    </Switch>
)
export default Main