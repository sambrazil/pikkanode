import React from 'react'

class Pikka  extends React.Component{
    state = {
        isLoading: true,
        mounted:true,
         id:'',
         caption:'',
         created_at:'',
         created_by:''
       
    }
    
   async componentDidMount(){
       if(this.state.mounted){
       try{
           const id = this.props.match.params.id
          const response = await fetch('http://localhost:3001/api/v1/pikka/'+id)
          const data = await response.json()
          data.list.map(p=>(
             this.setState({
                id:p.id,
                caption: p.caption,
                created_at: p.created_at,
                created_by: p.created_by
             })
          ))
       }
       catch(err){
           console.error(err)
       }
       finally{
           this.setState({
              isLoading: false
           })
       } 
    }
}
   componentWillUnmount(){
       this.setState({
           mounted:false
       })
   }
    render(){
        const {isLoading,id,caption,created_at,created_by} = this.state
             return(
                 <div>
                {isLoading?<img src='/images/loading.jpg'/>:<div></div>}  
                  <div>
                 <img src={'http://localhost:3001/images/'+id} width='100' height='100' /><br/>
                 id: {id}<br/>
                 caption: {caption} <br/>
                 created_at: {created_at} <br/>
                 created_by: {created_by}
                  </div>
                 </div>
             )
    }
}
export default Pikka