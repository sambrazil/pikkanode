import React from 'react'
import Pikka from './Pikka'
import {Switch,Route} from 'react-router-dom'
const Main =()=>(
    <Switch>
        <Route path='/pikka/:id' exact component={Pikka} />
    </Switch>
)
export default Main