const Koa = require('koa')
const app = new Koa()
let queue =[]

//Middleware 1
app.use( async(ctx,next)=>{
  queue.push('111')
  ctx.variable ='middleware1'
  console.log('Push Data: '+queue)
  await next()
})

//Middleware 2
app.use( async(ctx,next)=>{
    if(ctx.variable === 'middleware1'){
        queue.push('222')
        ctx.variable ='middleware2'
        console.log('Push Data: '+queue)  
        await next() 
    }
    else{
        await next()
    }
   
  })

  //Middleware 3
app.use( async(ctx,next)=>{
    if(ctx.variable === 'middleware2'){
        queue.push('333')
        console.log('Push Data: '+queue)
        console.log('Queue1: '+queue.shift())  
        console.log('Queue2: '+queue.shift()) 
        console.log('Queue3: '+queue.shift()) 
        await next() 
    }
    else{
        await next()
    }
   
  })

app.listen(3000)