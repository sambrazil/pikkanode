const Koa = require('koa');
const app = new Koa();
const Router = require('koa-router');
const router = new Router();
const mysql2 = require('mysql2/promise');
const render = require('koa-ejs');
const path = require('path');

render(app, {
    root: path.join(__dirname, 'views'),
    layout: false,
    viewExt: 'ejs',
    cache: false
  });
// const myModule = require('./my_module');
// const myModule2 = require('./my_module');
// console.log(myModule); // print 1 
// console.log(myModule2); // print 1 

const pool = mysql2.createPool({
    host: 'localhost',
    user: 'root',
    database: 'design_pattern',
});

router.get('/hello/', async (ctx, next) => {
    ctx.body = 'Hello Turtle !';
    await next();
});

router.get('/hello/:name', async (ctx, next) => {
    ctx.body = 'Hello '+ctx.params.name+' !';
    await next();
});

router.get('/hello/:name/:lastname', async (ctx, next) => {
    ctx.body = 'Fullname '+ctx.params.name+' '+ctx.params.lastname+' !';
    await next();
});

router.get('/normal_query/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * from users WHERE id = ? ', [ctx.params.id]);
    ctx.body = rows;
    await next();
});

router.get('/script_test/', async (ctx, next) => {
    await ctx.render('test', {
        myVariable: ['<strong>test text</strong>','<strong>test text2</strong>','<strong>test text3</strong>']
    });
    await next();
});

app.use(router.routes());
app.listen(3000);