const Koa = require('koa')
const Router = require('koa-router')
const app = new Koa()
const router = new Router()
/*
app.use(async (ctx, next) => {
    console.log(ctx.path + ' : 1');
    await next();
    console.log(ctx.path + ' : 6');
})
app.use(async (ctx, next) => {
    console.log('2');
    await next();
    console.log('5');
})
app.use(async (ctx, next) => {
    console.log('3');
    await next();
    console.log('4');
})
*/
router.get('/', async (ctx, next) => {
    ctx.myVariable = 'forbidden'; // assume variable
    //await next();
})
router.get('/special_area', async (ctx, next) => {
    ctx.myVariable = 'allow'; // assume variable
    //await next();
})
 async function myMiddleware (ctx, next) {
    if (ctx.myVariable == 'forbidden')
        ctx.body = "Access Denied";
    else
        await next();
 }
 app.use(myMiddleware)
 app.use(router.routes())
 app.use(myMiddleware)
 app.use(myMiddleware)

app.use(router.allowedMethods())
app.listen(3000)