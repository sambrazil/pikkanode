const {Employee} = require('./employee.js');
let fs = require('fs');

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        let self = this;
        fs.readFile('demofile1.txt','utf8',function(err, res) {
             console.log(self.lastname); // ok
             console.log(this.lastname); // TypeError: Cannot read property 'lastname' of undefined            
        }.bind(this))
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    hello() { // simulate public method
        console.log("Hi, nice to meet you. "+this.firstname+"!");
    }
}

exports.CEO = CEO;