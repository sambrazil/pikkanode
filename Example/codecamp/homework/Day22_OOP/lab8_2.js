class Employee {
    constructor(firstname, lastname, salary) {
        this._salary = salary; // simulate private variable

        this.firstname = firstname;
        this.lastname = lastname;
    }
    setSalary(newSalary) { // simulate public method
        if (newSalary > this._salary) {
            this._salary = newSalary;
            return newSalary;
        } else
            return false;
    }
    getSalary () {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leave(year, month, day) {
        console.log('I will leave on '+year+'-'+month+'-'+year)
    }
    gossip(employee, message) {
        console.log('Hey '+employee.firstname+', '+message);
    }
}

class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';

        /*
        let fire = (employee) => { // simulate private method
            console.log(employee.firstname + " has been fired!");
            this.dressCode = 'tshirt';
        };
        let hire = (employee) => { // simulate private method
            console.log(employee.firstname + " has been hired back!");
            this.dressCode = 'tshirt';
        }
        let seminar = () => { // simulate private method
            console.log("He is going to seminar");
            this.dressCode = 'suit';
        };
        let golf = () => { // simulate private method
            console.log("He goes to golf club to find a new connection.");
            this.dressCode = 'golf_dress';
        };
        this.work = (employee) => {  // simulate public method
            fire(employee);
            hire(employee);
            seminar();
            golf();
        }
        */
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    _fire (employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " has been fired!" + " Dress with :" + this.dressCode);
        
    }
    _hire (employee) { // simulate private method
        this.dressCode = 'tshirt';
        console.log(employee.firstname + " has been hired back!" + " Dress with :" + this.dressCode);
        
    }
    _seminar () { // simulate private method
        this.dressCode = 'suit';
        console.log("He is going to seminar" + " Dress with :" + this.dressCode);
        
    };
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
        
    };
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary) {
        let result = employee.setSalary(newSalary);
        if ( result )
            console.log(employee.firstname+"'s salary has been set to "+somsri.getSalary());
        else
            console.log(employee.firstname+"'s salary is less than before!!");
        return result;
    }

}

let somchai = new CEO("Somchai","Sudlor",30000);
let somsri = new Employee("Somsri","Sudsuay",22000);
somchai.gossip(somsri,"Today is very cold!");
somchai.work(somsri);

somchai.increaseSalary(somsri, 20);
somchai.increaseSalary(somsri, 25000);
