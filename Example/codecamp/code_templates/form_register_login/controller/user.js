module.exports = function (userModel, pool) {
    return {
        async profile (ctx, next) {
            const userId = ctx.params.id ? ctx.params.id : ctx.session.userId;
            const data = await userModel.getUserInfo(pool, userId);
            
            await ctx.render('profile', {
                userId: data.userId,
                email: data.email
            });
            await next();
        }
    }
}