const Koa = require('koa');
const app = new Koa();
const Router = require('koa-router');
const router = new Router();
const mysql2 = require('mysql2/promise');

const pool = mysql2.createPool({
    host: 'localhost',
    user: 'root',
    database: 'design_pattern',
});

const userModel = require('./models/user');

// No pattern
router.get('/normal_query/:id', async (ctx, next) => {
    const [rows] = await pool.query('SELECT * from users WHERE id = ?',[ctx.params.id]);
    ctx.body = rows;
    await next();
});

// Repository pattern
router.get('/find_user_by_id/:id', async (ctx, next) => {
    ctx.body = await userModel.findById(pool, ctx.params.id);
    await next();
});

router.get('/find_user_all/', async (ctx, next) => {
    ctx.body = await userModel.findAll(pool);
    await next();
});

// Write your own Repository pattern
router.get('/find_user_by_firstname/:firstname', async (ctx, next) => {
    // TODO: include repository
});

// Write your own Repository pattern
router.get('/remove_user/:id', async (ctx, next) => {
    // TODO: include repository
});

router.get('/add_user_certification/:id/:certificationName', async (ctx, next) => {
    let userObject = await userModel.findById(pool, ctx.params.id);
    
    userObject.money -= 3000;
    userObject.jsonData.certification.push({
        name: ctx.params.certificationName,
        year: 2018
    });
    await userModel.store(pool, userObject);

    ctx.body = userObject;
    await next();
});

app.use(router.routes());
app.listen(3000);