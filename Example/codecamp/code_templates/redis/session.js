const Koa = require('koa');
const app = new Koa();
const redis = require("redis");
client = redis.createClient();
const {promisify} = require('util');
const getAsync = promisify(client.get).bind(client);
const setAsync = promisify(client.set).bind(client);
const delAsync = promisify(client.del).bind(client);
const session = require("koa-session");

const sessionConfig = {
  key: "codekit",
  maxAge: 1000 * 60 * 60 * 24 * 7,
  httpOnly: true /** (boolean) httpOnly or not (default true) */,
  store: {
    async get(key, maxAge, { rolling }) {
      return JSON.parse( await getAsync(key) );
    },
    async set(key, sess, maxAge, { rolling }) {
      await setAsync(key, JSON.stringify(sess));
    },
    async destroy(key) {
      await delAsync(key);
    }
  }
}

app.keys = ['some secret codecamp yeah!'];
app.use(session(sessionConfig, app));
app.use(async (ctx, next) => {
  ctx.session.count = ctx.session.count ? ctx.session.count + 1 : 1;
  ctx.body = ctx.session.count;
  console.log(ctx.path);
  await next();
});

app.listen(3000);