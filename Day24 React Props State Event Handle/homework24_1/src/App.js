import React from 'react'
import PictureCard from './PictureCard'

const App =()=>{
    return(
        <div>
        <div style ={{color:'red'}}>
            <PictureCard
            imgSrc ='/images/1.jpg'
            id ={1}
            createBy='AAAA'
            date ='1-01-01'
            likeCount={1}
            countComment={1}
            /></div>,
             <PictureCard
           imgSrc ='/images/2.jpg'
            id ={2}
            createBy='BBB'
            date ='2-02-02'
            likeCount={2}
            countComment={2}
            />,
             <PictureCard
            imgSrc ='/images/3.jpg'
            id ={3}
            createBy='CCCC'
            date ='3-03-03'
            likeCount={3}
            countComment={3}
            />,
             <PictureCard
            imgSrc ='/images/4.png'
            id ={4}
            createBy='DDDD'
            date ='4-04-04'
            likeCount={4}
            countComment={4}
            />,
             <PictureCard
            imgSrc ='/images/5.jpg'
            id ={5}
            createBy='EEEE'
            date ='5-05-05'
            likeCount={5}
            countComment={5}
            />
        </div>
    )
}
export default App
