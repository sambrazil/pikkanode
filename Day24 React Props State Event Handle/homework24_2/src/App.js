import React from 'react'
class App extends React.Component{
state = {
    email:'',
    gender:'',
    title:'',
   firstName:'',
   lastName:'',
   pictureURL:''
}

fetchAPI = (e)=>{
fetch('https://randomuser.me/api/',)
.then(res=> res.json())
.then(data=>{
    this.setState({
        email:data.results[0].email,
        gender:data.results[0].gender,
        title:data.results[0].name.title,
       firstName: data.results[0].name.first,
       lastName:data.results[0].name.last,
       pictureURL:data.results[0].picture.large

    })


})
.catch(err=> console.error(err))
}
    render(){
        return(
            <div>
             <img src={this.state.pictureURL}/><br/>
             email:{this.state.email}<br/>
             gender:{this.state.gender}<br/>
             {this.state.title} {this.state.firstName} {this.state.lastName}<br/>   
            <button onClick={this.fetchAPI}>Generate User</button>
            </div>
        )
    }
}
export default App