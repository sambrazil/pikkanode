module.exports = function (userModel, pool) {
    return {
        async usernameExisted(username) {
            const [usernameQuery] = await pool.query(`
                SELECT username FROM user
                WHERE username = ?
            `, [username]);
            if ( usernameQuery[0].username ) {
                return true;
            } else {
                return false;
            }
        },
        async emailExisted(email) {
            const [emailQuery] = await pool.query(`
                SELECT email FROM user
                WHERE email = ?
            `, [email]);
            
            if ( emailQuery[0].email) {
                return true;
            } else {
                return false;
            }
        },
        async insertUser(pool, username, password, email) {
            const aData = [
                username, 
                this.stupidHash(password),
                email,
                
            ];
            const sql = `INSERT INTO user (username, password, email) VALUES (?, ? ,?)`;
            const [resultInsert] = await pool.query(sql, aData);
        
            return resultInsert.insertId;
        },
       stupidHash(password) {
            return '123' + password + '123';
        }
    }
}