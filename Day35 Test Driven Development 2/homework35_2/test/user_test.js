const pool = {
    query:()=>{}
}
const assert = require('assert')
const sinon = require('sinon')
const tempUserController = require('../controller/user')
const userController = tempUserController(null, pool)

describe('userController', ()=>{
    describe('#usernameExisted()', ()=>{
        it('Already have username (return true)', async ()=>{
        const ctx = {
            request:{
                body:{
                    username:'1111'
                }
            }
        }
      const poolStub = sinon.stub(pool, 'query').resolves([[{
        username:'2222'
      }]]) 
      const dataSend = await userController.usernameExisted(ctx.request.body.username)
      console.log('return is '+dataSend)
      poolStub.restore()
     sinon.assert.calledOnce(poolStub)
     assert.equal(dataSend,true)
    }),
    it('no have username return(false)', async()=>{
        const ctx = {
            request:{
                body:{
                    username:'1111'
                }
            }
        }
       const poolStub = sinon.stub(pool, 'query').resolves([[{
             username:undefined
       }]]) 
       const dataSend = await userController.usernameExisted(ctx.request.body.username)
       console.log('return is '+dataSend)
       poolStub.restore()
       sinon.assert.calledOnce(poolStub)
       assert.equal(dataSend,false)
    })
    })

})

describe('userController', ()=>{
    describe('#emailExisted()', ()=>{
        it('already have email  (return true)', async()=>{
            const ctx = {
                request:{
                    body:{
                        email:'1@1'
                    }
                }
            }
           const poolStub = sinon.stub(pool,'query').resolves([[{
               email:'2@2'
           }]]) 
           const dataSend = await userController.emailExisted(ctx.request.body.email)
           poolStub.restore()
           sinon.assert.calledOnce(poolStub)
           console.log('return is '+dataSend)
           assert.equal(dataSend,true)
        }),
        it('no have email  (return false)', async()=>{
            const ctx = {
                request:{
                    body:{
                        email:'1@1'
                    }
                }
            }
           const poolStub = sinon.stub(pool,'query').resolves([[{
               email:undefined
           }]]) 
           const dataSend = await userController.emailExisted(ctx.request.body.email)
           poolStub.restore()
           sinon.assert.calledOnce(poolStub)
           console.log('return is '+dataSend)
           assert.equal(dataSend,false)
        })
    })
})

describe('userController', ()=>{
    describe('#insertUser', ()=>{
        it('sucess insert', async ()=>{
        const ctx = {
            request:{
               body:{
                username:'1', 
                password:'1', 
                email:'1@1'
               }
            }}
      const poolStub = sinon.stub(pool, 'query').resolves([[{
        username:'user1', 
        password:'1', 
        email:'1@1'
      }]])
    await userController.insertUser(pool,ctx.request.body.username, ctx.request.body.password, ctx.request.body.email) 
      poolStub.restore()
     sinon.assert.calledOnce(poolStub)
        })
    })
})
