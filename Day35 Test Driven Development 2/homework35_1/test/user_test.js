const assert = require('assert')
const sinon = require('sinon')
const pool = {query:()=>{}}
const tempUserController = require('../controller/user')
const userController = tempUserController(null, pool)

describe('userController', ()=>{
    describe('#insertUser()', ()=>{
        it('Success insert', async ()=>{
            const ctx = {
                request:{
                    body:{
                        username:'test',
                        password:'1',
                        email:'1@1'
                    }
                },
                render: async ()=>{}
            }
            const next = sinon.spy(userController,'next')
           const poolStub = sinon.stub(pool, 'query').resolves([[{
                
           }]])
           const dataSend = await userController.insertUser(ctx,next)
           poolStub.restore()
           next.restore()
           sinon.assert.calledOnce(poolStub)
           // console.log(dataSend)
          assert.deepEqual(dataSend.errorMessage,'')
        })
    })
})