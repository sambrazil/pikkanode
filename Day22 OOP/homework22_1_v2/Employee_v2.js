class Employee {
    constructor(firstname, lastname, salary) {
        this._salary = salary; // simulate private variable
        this.firstname = firstname
    }
    setSalary(newSalary, oldSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
        if(newSalary > oldSalary){
            return newSalary
        }
        else if (newSalary < oldSalary){
            return false
        }
    }
    getSalary () {  // simulate public method
        return this._salary;
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}
module.exports.Employee = Employee