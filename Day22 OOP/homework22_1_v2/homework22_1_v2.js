const {CEO} = require('./CEO_v2')
const {Employee} = require('./Employee_v2')
let somchai = new CEO("Somchai","Sudlor",30000);
let somsri = new Employee("Somsri","Sudsuay",22000);
somchai.gossip(somsri.firstname,"Today is very cold!");
somchai.work(somsri.firstname);

somchai.increaseSalary(somsri.firstname, 20, somsri._salary);
somchai.increaseSalary(somsri.firstname, 25000, somsri._salary);