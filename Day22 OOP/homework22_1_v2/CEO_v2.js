const {Employee} = require('./Employee_v2')
class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
    }
    getSalary(){  // simulate public method
        return super.getSalary()*2;
    };
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary, oldSalary) {
        if(super.setSalary(newSalary, oldSalary) == false){
            console.log(`${employee}'s salary is less than before!!`)
        }
        else if(super.setSalary(newSalary, oldSalary) == newSalary){
            console.log(`${employee}'s salary has been set to ${newSalary}`)
        }
    }
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
        
    };
    gossip(employee, message){
        console.log(`Hey ${employee} ${message}`)
    }
    _fire(employee){
      this.dressCode = 'tshirt'
      console.log(`${employee} has been fired! Dress with :${this.dressCode}`)
    }

    _hire(employee){
       this.dressCode = 'tshirt'
       console.log(`${employee} has been hired back! Dress with :${this.dressCode}`)
    }

    _seminar(){
        this.dressCode = 'suit'
        console.log(`He is going to seminar Dress with :${this.dressCode}`)
    }
}
module.exports.CEO = CEO
