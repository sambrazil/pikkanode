const {MobilePhone} = require('./MobilePhone')

class SamSungGalaxyNote8 extends MobilePhone {
    constructor(){
        super()
    }
    UseGearVR(){
        console.log('SamSungGalaxyNote8 UseGearVR')
    }
    TransformToPC(){
        console.log('SamSungGalaxyNote8 TransformToPC')
    }
    UsePen(){
        console.log('SamSungGalaxyNote8 UsePen')
    }
    GooglePlay(){
        console.log('SamSungGalaxyNote8 GooglePlay')
    }
} 
module.exports.SamSungGalaxyNote8 = SamSungGalaxyNote8