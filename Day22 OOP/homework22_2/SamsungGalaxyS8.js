const {MobilePhone} = require('./MobilePhone')

class SamSungGalaxyS8 extends MobilePhone {
    constructor(){
        super()
    }
    UseGearVR(){
        console.log('SamSungGalaxyS8 UseGearVR')
    }
    TransformToPC(){
        console.log('SamSungGalaxyS8 TransformToPC')
    }
   
    GooglePlay(){
        console.log('SamSungGalaxyS8 GooglePlay')
    }
} 
module.exports.SamSungGalaxyS8 = SamSungGalaxyS8