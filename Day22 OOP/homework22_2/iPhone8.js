const {MobilePhone} = require('./MobilePhone')
class iPhone8 extends MobilePhone{
    constructor(){
        super()
    }
    TouchID(){
        console.log('iPhone8 TouchID')
    }
    AppStore(){
        console.log('iPhone8 AppStore')
    }
}
module.exports.iPhone8 = iPhone8