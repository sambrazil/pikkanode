class Employee {
    constructor(firstname, lastname, salary) {
        let _salary = salary; // simulate private variable
        this.getSalaryPrivate = function(){
            return _salary
        }
        this.firstname = firstname

    }
    setSalary(newSalary, salary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary
        if(newSalary > salary){
            return newSalary
        }
        else{
            return false
        }
    }
    getSalary () {  // simulate public method
        return this.getSalaryPrivate()
    };
    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
    gossip(employee,message){
        let gossip = `Hey ${employee} ${message}`
        console.log(gossip)
    }
}
module.exports.Employee = Employee