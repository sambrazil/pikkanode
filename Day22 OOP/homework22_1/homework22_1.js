const {Employee} = require('./employee')
const {CEO} = require('./CEO')

let somchai = new CEO("Somchai","Sudlor",30000);
let somsri = new Employee("Somsri","Sudsuay",22000);
somchai.gossip("Somsri","Today is very cold!");
somchai.work("Somsri");

somchai.increaseSalary("Somsri", 20, somsri.getSalary());
somchai.increaseSalary("Somsri", 25000, somsri.getSalary());

//console.log(somsri.getSalary())
//console.log(somchai.getSalary())
