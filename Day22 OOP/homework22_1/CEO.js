const {Employee} = require('./employee')
class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary);
        this.dressCode = 'suit';
        
    }
    getSalary(){  // simulate public method
      return super.getSalary()
    };
    work (employee) {  // simulate public method
        this._fire(employee);
        this._hire(employee);
        this._seminar();
        this._golf();
    }
    increaseSalary(employee, newSalary, salary) {
       if(super.setSalary(newSalary, salary) > salary){
           console.log(`${employee}'s salary has been set to ${newSalary}`)
       }
       else {
        console.log(`${employee}'s salary is less than before!!`)
    }
    //console.log(super.setSalary(newSalary, salary))

  }
    _golf () { // simulate private method
        this.dressCode = 'golf_dress';
        console.log("He goes to golf club to find a new connection." + " Dress with :" + this.dressCode);
        
    };

 _fire(employee){
    this.dressCode = 't-shirt'
    let message = `${employee} has been fired! Dress with :${this.dressCode}` 
    console.log(message)
  }

  _hire(employee){
    this.dressCode = 't-shirt'
    let message  = `${employee} has been hired back! Dress with :${this.dressCode}` 
    console.log(message)
  }

  _seminar(){
    this.dressCode = 'suit';
    let message= `He is going to seminar Dress with :${this.dressCode}` 
    console.log(message)
  } 
}
module.exports.CEO = CEO