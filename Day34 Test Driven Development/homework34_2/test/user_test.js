const mysql = require('mysql2/promise');
const pool = mysql.createPool({
    host:'localhost', 
    user: 'root', 
    database: 'koa_login'
});
const tempUserController = require('../controller/user')
const userController = tempUserController(null, pool)
const assert = require('assert')

describe('userController', ()=>{
    describe('#usernameExisted()', ()=>{
       it('already have this username in database (return true)', async ()=>{
        const ctx ={
            request:{
                body:{
                     username:'myUsername'
                }
            }
        }
        const data = await userController.usernameExisted(ctx.request.body.username)
        assert.equal(data,true)
       }),
       
       it('not have this username in database (return false)', async ()=>{
        const ctx ={
            request:{
                body:{
                     username:'ssdfdfdsfsdff'
                }
            }
        }
        const data = await userController.usernameExisted(ctx.request.body.username)
        assert.equal(data,false)
       })
    })
})

describe('userController', ()=>{
    describe('#emailExisted()', ()=>{
        it('already have email in database (return true)', async ()=>{
            const ctx = {
                request:{
                    body:{
                        email:'myUsername@myUsername'
                    }
                }
             
            }
            const data = await userController.emailExisted(ctx.request.body.email)
            assert.equal(data,true)
        }),

        it('not have email in database (return false)', async ()=>{
            const ctx = {
                request:{
                    body:{
                        email:'sdassddssdds'
                    }
                }
             
            }
            const data = await userController.emailExisted(ctx.request.body.email)
            assert.equal(data,false)
        })
    })
})

describe('userController', ()=>{
    describe('#insertUser()', ()=>{
        it('success insert data (insertId not null)', async ()=>{
            const ctx = {
                request:{
                    body:{
                        username:'testUser',
                        password:'1234',
                        email:'test@test'
                    }
                }
            }
           const data = await userController.insertUser(pool, ctx.request.body.username, ctx.request.body.password, ctx.request.body.email)
            assert.notEqual(data,null)

        }),

        it('fail insert data (insertId is null)', async ()=>{
            const ctx = {
                request:{
                    body:{
                        username:'',
                        password:'',
                        email:''
                    }
                }
            }
           const data = await userController.insertUser(pool, ctx.request.body.username, ctx.request.body.password, ctx.request.body.email)
            assert.equal(data,null)

        })
    })
})




