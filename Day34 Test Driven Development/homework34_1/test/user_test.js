const mysql = require('mysql2/promise');
const pool = mysql.createPool({
    host:'localhost', 
    user: 'root', 
    database: 'koa_login'
});

const tempUserController = require('../controller/user')
const userController = tempUserController(null,pool)
const assert = require('assert')


describe('userController', ()=>{
    describe('#insertUser()', ()=>{
        it('should return empty errorMessage', async ()=>{
            const ctx = { 
                request:{
                    body:{
                     username:'myUsername',
                     email:'myUsername@myUsername',
                     password:'111111'
                    }          
                },
                render: async function(somePage, dataObject){
        
                }
               }
        
               async function next(){
                   return true;
               }
        
         const dataSend = await userController.insertUser(ctx, next)
         assert.deepEqual(dataSend.errorMessage,'')
        })
    })
})