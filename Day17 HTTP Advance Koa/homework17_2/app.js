const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const koaBody = require('koa-body')
const mysql = require('mysql2/promise')
const uuidv4 =require('uuid/v4')
const fs = require('fs-extra')
const path = require('path')
const serve = require('koa-static')

const app = new Koa()
const router = new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
  })
 const pool = mysql.createPool({
     host:'localhost',
     user:'root',
     password:'',
     database:'pikkanode'
 }) 
 
router.get('/create',uploadGetHandler)
router.post('/create',uploadPostHandler)
router.get('/', async ctx=>{
    const [rows] = await pool.query('select * from pictures')
    const dataEJS ={'picture':rows}
    await ctx.render('index',dataEJS)
})

async function uploadGetHandler(ctx){
 await ctx.render('upload')
}

async function  uploadPostHandler(ctx){
    const fileName = uuidv4() + ctx.request.files.photo.name
    const folderDir = path.join(__dirname, 'public/images')
    const fileDir = path.join(folderDir, `${fileName}` )
    await fs.rename(ctx.request.files.photo.path, fileDir)
    const caption = ctx.request.body.caption
    await pool.query(`insert into pictures (id, caption, created_by) values ('${fileName}', '${caption}', 1)`)
}

app.use(koaBody({multipart:true}))
app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)