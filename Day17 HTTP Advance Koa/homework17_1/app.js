const Koa = require('koa')
const Router = require('koa-router')
const mysql = require('mysql2/promise')
const render = require('koa-ejs')
const path = require('path')
const koaBody = require('koa-body')

const app = new Koa()
const router =new Router()
render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false,
  
  })
const pool = mysql.createPool({
    host:'localhost',
    user:'root',
    password:'',
    database:'pikkanode'
})
 
router.get('/signin',signinGetHandler)
router.post('/signin',signinPostHandler)

router.get('/signup',signupGetHandler)
router.post('/signup',signupPostHandler)

async function signinGetHandler(ctx){
    await ctx.render('form_sign_in')
}
function signinPostHandler(ctx){
console.log('email:'+ctx.request.body.email)
console.log('password:'+ctx.request.body.password)
ctx.status=302
ctx.redirect('/signin')

}

async function signupGetHandler(ctx){
    await ctx.render('form_sign_up')
}
async function signupPostHandler(ctx){
    const email = ctx.request.body.email
    const password = ctx.request.body.password
    const confirm_password = ctx.request.body.confirm_password
    console.log('email:'+email)
    console.log('password:'+password)
    console.log('confirm password'+confirm_password)
    await pool.query(`INSERT INTO users(email, password)  VALUES('${email}', '${password}')`)
    ctx.status = 302
    ctx.redirect('/signup')
}
app.use(koaBody()) 
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)