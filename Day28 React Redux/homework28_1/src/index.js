import React from 'react'
import ReactDOM from 'react-dom'
import 'todomvc-app-css/index.css'
import App from './components/App'
import { createStore } from 'redux';
import Reducer from './reducer'
import {Provider} from 'react-redux'
// TODO: Create Store here
const store = createStore(Reducer)
// TODO: Add Provider
ReactDOM.render(<Provider store={store}><App /></Provider>, document.getElementById('root'))
