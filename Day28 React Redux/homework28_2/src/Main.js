import React from 'react'
import SignIn from './SignIn'
import {Switch,Route} from 'react-router-dom'
const Main =()=>(
    <Switch>
        <Route path='/signin' exact component={SignIn} />
    </Switch>
)
export default Main