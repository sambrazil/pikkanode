import React from 'react'
import {connect} from 'react-redux'
import * as actions from './actions'

class SiginIn extends React.Component{
 state = {
     email:'',
     password:''
 }
 HandlerOnchange = e =>{
     this.setState({
         [e.target.name]:e.target.value
     })
 }
 HandlerSubmit = async e =>{
     e.preventDefault()
    const {email,password} = this.state
    if(email===''){
        alert('no email')
        document.form.email.focus()
        return
    }
    if(password===''){
        alert('no password')
        document.form.password.focus()
        return
    }
    try{
     const option = {
          method:'post',
          headers:{'content-type':'application/json'},
          body: JSON.stringify({
              email:email,
              password:password
          }) 
      }
       const response = await fetch('http://localhost:3001/api/v1/auth/signin',option)
       const data = await response.json()
       const status = await response.status
       console.log(data)
       if(status==200){
         this.props.authStatus()
       }

    }
    catch (err){
        console.error(err)
    }
 }
render(){
    const {email,password} = this.state
    
    return(
        <form method="POST" name='form' onSubmit={this.HandlerSubmit}>
        <label htmlFor='email'>email</label>
        <input type="email" name="email" id='email' value={email} onChange={this.HandlerOnchange} />
        <br/>
        <label htmlFor='password'>password</label>
        <input type="password" name='password' id='password' value={password} onChange={this.HandlerOnchange} />
        <br/>
        <button>submit</button><br/>
        {this.props.message}
    </form>
    )

 }
}
const MapStateToProbs = state =>(
    {...state}
)
export default connect(MapStateToProbs,actions)(SiginIn)