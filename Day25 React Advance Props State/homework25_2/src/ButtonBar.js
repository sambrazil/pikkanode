import React from 'react'
import PropTypes from 'prop-types'
import Button from './Button'

const ButtonBar = (props) => (
  <div className="d-flex flex-row">
    <Button btnType='btn-success' btnIcon='fa fa-plus' onClick={props.incrementCount}/>
    <Button btnType='btn-warning' btnIcon='fa fa-refresh' onClick={props.resetCount}/>
    <Button btnType='btn-danger' btnIcon='fa fa-minus'onClick={props.decrementCount}/>
  </div>
)
ButtonBar.propTypes ={
  incrementCount:PropTypes.func,
  decrementCount:PropTypes.func,
  resetCount:PropTypes.func  
}
export default ButtonBar
