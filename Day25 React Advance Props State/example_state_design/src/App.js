import React from 'react'
import AddForm from './AddForm'
import ListItem from './ListItem'

class App extends React.Component{
 state = {
   nextId:1,
   products:[{
    name:'1111',
    price:'1111'
   }]
 }
 onSubmit= e=>{
   const {nextId,products} = this.state
   this.setState({
     products:[...products,
      {
       id:nextId,
       name:e.name,
       price:e.price
     }],
     nextId:nextId+1
   })
 }
    render(){
      return(
        <div>
          <AddForm onSubmit={this.onSubmit} />
          <ListItem products={this.state.products}/>
        </div>
      )
    }
}
export default App