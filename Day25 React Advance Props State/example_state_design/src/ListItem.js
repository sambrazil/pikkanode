import React from 'react'
import Item from './Item'
const ListItem =({products})=>(
  <div>
 { products.map(p=>(
    <Item key={p.id} product={p} />
  ))}
  </div>
)
export default ListItem