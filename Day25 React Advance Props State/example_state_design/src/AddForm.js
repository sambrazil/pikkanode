import React from 'react'
class AddForm extends React.Component{
  state = {
    name:'',
    price:''
  }
  HandleChange = e=>{
    this.setState({
      [e.target.name]:e.target.value
    })
  }
 HandleSubmit = e=>{
   e.preventDefault()
   this.props.onSubmit(this.state)
   this.setState({
    name:'',
    price:''
   })
 } 
 render(){
   const {name, price} = this.state
   return(
     <div>
     <form onSubmit={this.HandleSubmit}>
       <label htmlFor='name'>name</label><input type='text' id='name' name='name' value={name} onChange={this.HandleChange}/><br/>
       <label htmlFor='price'>price</label><input type='text'  id='price' name='price' value={price}  onChange={this.HandleChange}/><br/>
       <button>Add</button>
     </form>
     </div>
   )
 }
}
export default AddForm